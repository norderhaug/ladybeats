# LadyBeats

A universal (nee isomporhic) Clojurescript React app for mobile and/or web.

Runs on Docker and Heroku using node express, bootstrap, and material design, with hotloaded code shared between frontend and backend.

## References

https://clojure.org/
https://clojurescript.org/
https://reagent-project.github.io/
https://getbootstrap.com/docs/4.0/layout/overview/
http://www.material-ui.com
https://expressjs.com/

## Deploy with Docker

Start a local web server in a Docker container:

    docker-compose up release

Access http://localhost:5000 from a browser.

## Run Locally

Requirements: leiningen, heroku, npm

To start a server on your own computer:

    lein do clean, deps, compile
    heroku local web

Point your browser to the displayed local port.
Click on the displayed text to refresh.

## Deploy Alexa skill

    lein cljs-lambda default-iam-role
    lein cljs-lambda deploy

## Deploy to Heroku

To start a server on Heroku:

    heroku apps:create
    git push heroku master
    heroku open

This will open the site in your browser.

## License

Copyright © 2018 Terje Norderhaug

Distributed under the Eclipse Public License either version 1.0
or (at your option) any later version.
