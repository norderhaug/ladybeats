(ns app.dashboard.main
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [app.dashboard.pane
    :refer [pane]]))

(defn report-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title "Reported Symptoms"]]
   [:div ;.card-body
    [:ul.list-group
     [:li.list-group-item "Exhaustion"]
     [:li.list-group-item "Out of Breath"]
     [:li.list-group-item "Can't do regular activites"]]]])

(defn compliance-card [session]
  [:div.card
   [:div.card-header
     [:div.card-title "Adherence Check"]]
   [:div.card-body
    [:div "Patient indicates compliance with medications."]]])

(defn telehealth-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title "Telehealth Visits"]]
   [:div.card-body
    [:div "Pending scheduled with A. Jones RN"]]])

(defn symptoms-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title "Symptom Overview"]]
   [:div ; .card-body
    [:ul.list-group
     [:li.list-group-item "Fatigue"]
     [:li.list-group-item "Out of Breath"]]]])

(defn medications-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title "Medications"]]
   [:div ; .card-body
    [:ul.list-group
     [:li.list-group-item "Lisinopril"]
     [:li.list-group-item "Atenolol"]
     [:li.list-group-item "Rosuvastatin"]]]])

(def juliet-img-url
  "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/33110831_10155359787127665_4072251522052784128_n.jpg?_nc_cat=0&oh=967f5c7ee3f5e45be99db5dd2e5e3f10&oe=5BD540C9")

(defn patient-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title
     [:h3
      [ui/avatar {:src juliet-img-url}]
      [:span {:style {:margin-left "1em"}}
       "Juliet M. Oberding"]
      [:span.badge.badge-pill.badge-primary {:style {:margin-left "0.5em"}}
       "3 msgs"]]]]])

(defn view [session]
  [:div.well
   [:div.card-deck
    [patient-card session]]
   [:div ; .card-deck {:style {:margin "1em"}}
    [:div.row {:style {:margin "1em"}}
     [:div.col.col-sm-4 {:style {:margin-bottom "1em"}}
      [report-card session]]
     [:div.col.col-sm-4 {:style {:margin-bottom "1em"}}
      [medications-card session]]
     [:div.col.col-sm-4 {:style {:margin-bottom "1em"}}
      [compliance-card session]]
     [:div.col.col-sm-4 {:style {:margin-bottom "1em"}}
      [telehealth-card session]]
     [:div.col-sm-8 {:style {:margin-bottom "1em"}}
      [symptoms-card session]]]]])
